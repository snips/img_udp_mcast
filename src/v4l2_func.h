#ifndef _V4L2_FUNC
#define _V4L2_FUNC

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <getopt.h>             /* getopt_long() */

#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <linux/videodev2.h>

#define CLEAR(x) memset(&(x), 0, sizeof(x))

#ifndef V4L2_PIX_FMT_H264
#define V4L2_PIX_FMT_H264     v4l2_fourcc('H', '2', '6', '4') /* H264 with start codes */
#endif

//STRUCTURES
enum io_method {
        IO_METHOD_READ,
        IO_METHOD_MMAP,
        IO_METHOD_USERPTR,
};

struct buffer {
        void   *start;
        size_t  length;
};


//GLOBAL VARS
char            *dev_name;
enum io_method   io;
int              fd;
struct buffer          *buffers;
unsigned int     n_buffers;
int              out_buf;
int              force_format;
int              frame_count;
int              frame_number;

//FUNC DECLARATIONS
void errno_exit(const char *s);
void process_image(const void *p, int size);
int read_frame(struct v4l2_buffer* buf);
void enque_buffer(struct v4l2_buffer* buf);

void open_device(void);
void init_device(void);
void start_capturing(void);
void grab_frame(struct v4l2_buffer* buf); //void mainloop(void);
void stop_capturing(void);
void uninit_device(void);
void close_device(void);

#endif