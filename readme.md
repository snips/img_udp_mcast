# Image UDP Multicast

## mcastserver  
The server will capture images from a USB camera using the v4l2 driver. It will then
transmit it frame by frame using UDP multicast to a pre-defined group.

## mcastclients  
There are TWO clients, both of which subscribe to the group. BOTH processes can simultaneously
receive the images and process them as required!

## Instructions to build  
- Open terminal and navigate to the `build` folder: ```$ cd <project loc>/build```
- Execute commands:
	- ```$ cmake ../src```
	- ```$ make```

To execute the program:
- Find the device ID of you USB camera that supports capturing jpeg images. Open a terminal and type:
```$ cd /dev``` ```$ ls```
you should be able to see the device displayed as videoX (where X is 0,1,2...)
- In the code, change variable `dev_name` to the name of your device (eg: `/dev/video0`). Then compile program.
- Open a terminal and navigate to the build folder: : ```$ cd <project loc>/build```
- Launch the client: ```$ ./mcastclient```
- Open a second terminal in same location and type: ```$ ./mcastclient2```
- Open a third terminal and launch the server: ```$ ./mcastserver```
- The `data` folder should get populated with 3 copies of each image, numbered identically,
from the server, client and client2 resp.

## Warning
- USB camera/webcam must support capturinng jpeg images.
- If the programs are launched in a location with no `data` folder, then it will crash and give an error.
- If the USB camera cannot be found then it will give error and exit
- Any other errors, like unable to Que buffers, etc will crash the program and give error code.
